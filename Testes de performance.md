# Testes de performance

Aqui ficará documentado os planos de testes voltados para a performance da API Serverest.
Eu irei rodar os testes na minha máquina local, as especificações de hardware são:
- Processador AMD Ryzen 7 5700U
- Memória RAM de 8gb (6gb disponíveis por causa da placa integrada)
- Placa integrada Radeon Graphics Vega 8 2gb


### Scripts individuais:
- Realizar Login
- Realizar Cadastro
- Realizar Cadastro de produtos
- Realizar busca de usuários
- Editar usuários
- Editar produtos
- Realizar busca de produtos
- Excluir usuários
- Excluir produtos

### Scripts coletivos
- Realizar cadastro -> Realizar login -> Buscar produtos
- Realizar cadastro -> Login -> Criar produtos -> Editar produtos
- Realizar Login -> Buscar usuários -> Editar usuário
- Realizar Login -> Buscar usuários -> Buscar produtos -> Excluir produtos
- Realizar Login -> Cadastrar produtos -> Excluir usuários

*Nota: Independentemente, a realização de um login é obrigatória para quase todos, exceto na busca de produtos/usuários e no cadastro de usuários*
*Nota nº2: Os fluxos 3 a 5 serão considerados apenas um usuário realizando a mesma ação para evitar lentidão*

## Volumetria
*Considerando uma carga X de usuários simultâneos por 10 minutos*


### Rota Login e Usuários
Decidi juntar ambos devido ao fato de envolverem os usuários em si, no cadastro e na validação.
- Começando pelo cadastro de usuários, irei começar com cargas baixas, de 10 a 20 usuários simultâneos devido ao meu hardware, e irei aumentando gradativamente até 100 usuários simultâneos dependendo do desempenho da minha máquina.
- Quanto ao login de usuários, usarei a mesma quantidade, aumentando gradativamente até 50 usuários simultâneos realizando o login.

- *Nota: Quanto às demais funcionalidades da rota usuários, vale o mesmo usado para a rota de login*

### Rota Produtos
- Irei começar com o cadastro de 20 produtos por segundo, aumentando gradativamente até 200 produtos por segundo.
- As buscas por produtos costumam ser frequentes, então irei usar uma carga de 50 usuários simultâneos inicialmente.
- O mesmo vale para os demais, 50 usuários simultâneos.

- *Nota: talvez eu tenha que usar a mesma carga no cadastro de produtos, devido ao Jmeter*

### Rota Carrinhos
- O cadastro de carrinhos é muito importante, por isso irei usar cargas de 45 a 50 usuários simultâneos e aumentar até um limite de 250 usuários.
- Há uma dualidade entre concluir a compra e cancelar a compra, principalmente na falta de uma forma de editar o carrinho após ele ser criado, usarei uma carga de 20 a 50 usuários simultâneos para ambos (concluir e cancelar).
-Para a listagem de carrinhos, usarei uma carga de 20 usuários simultâneos

## Métricas

Para todas as rotas, serão analisadas pelo menos três métricas, tendo a possibilidade de analisar outras durante o teste:

- Será registrado o tempo médio de resposta a cada teste gradativo.
- Irei verificar também a taxa de erro dos usuários que tentam se cadastrar ou logar na API
- Vou analisar também o uso de recursos, assim dependendo do uso de CPU, memória ou qualquer outra coisa que seja, posso determinar outro limite nos testes gradativos.

## O que será testado?

Como foi dito anteriormente, a API Serverest ou apenas Serverest, é uma api que simula uma e-commerce em um site de navegador, será o objeto deste teste.


## Resultados:

Decidi escolher uma das rotas para realizar os testes, a rota de usuários.

- Cadastro: Inicialmente houve alguns erros devido a alguns usuários cadastrando um email já cadastrado, por isso aumentei os parâmetros nas variáveis aleatórias.
Com 10 usuários (que não são os mesmos) realizando a operação de cadastro 10 vezes, ou seja, 100 amostras, não houve erros.
Aumentei agora para 30 usuários diferentes entre si e a cada iteração realizando o cadastro 100 vezes (3000 amostras), também não houve erros.

*nota: descobri que não há usuários o suficiente para realizar esse teste continuamente durante 10 minutos, talvez eu tenha que determinar um número infinito de iterações.*

Refiz o mesmo teste, agora com iterações infinitas durante 1 minuto, com 30 usuários simultâneos, obtive 6406 amostras, sem erros, uma taxa de 59,06 Kbps, o tempo de resposta médio foi de 265ms.
Visto que não ocorreu erros, farei agora com o tempo determinado, 10 minutos.
Neste teste de 10 minutos, já tivemos um resultado diferente, mesmo não havendo erros em nenhum momento, o tempo de resposta variou muito e aumentou gradativamente, entre 400ms e 900ms.

## Resultados dos Fluxos:

### Fluxo 1: Realizar cadastro -> Realizar login -> Buscar produtos
![Fluxo 1](<fotos/Captura de tela 2023-10-27 124843.png>)

No primeiro teste houve um aproveitamento de quase 80% de sucesso, sendo que aqueles que falharam recebiam o status 400, minha teoria é de que talvez seja o hardware e o sistema que não está suportando uma determinada quantidade de usuários realizando várias ações de uma vez em sua totalidade.

### Fluxo 2: Realizar cadastro -> Login -> Criar produtos -> Editar produtos
![Fluxo 2](</fotos/Captura de tela 2023-10-27 130457.png>)

Neste teste, apesar das semelhanças, houve uma taxa de 100% de sucesso e ainda com requisições a mais, o que pode invalidar a minha teoria citada antes.

### Fluxo 3: Realizar Login -> Buscar usuários -> Editar usuário
![Fluxo 3](<fotos/Captura de tela 2023-10-27 132245.png>)

A partir deste teste, seria considerado apenas um usuário repetindo a mesma ação várias vezes (ou vários usuarios usando as mesmas credenciais que realizam várias ações), neste caso foi algo mais condicional, porque para editar usuários, você precisa especificar um id, a maioria dos erros foram devidos a requisição de edição, no qual eu não coloquei parâmetros aleatórios para aplicar a edição, até porque isso dificultaria as próximas requisições.

### Fluxo 4: Realizar Login -> Buscar usuários -> Buscar produtos -> Excluir produtos
![Fluxo 4](</fotos/Captura de tela 2023-10-27 133204.png>)

Este funcionou sem menores problemas, também considerando as mesmas credenciais para login.

### Fluxo 5: Realizar Login -> Cadastrar produtos -> Excluir usuários
![Fluxo 5](<fotos/Captura de tela 2023-10-27 133455.png>)

Neste teste ocorreu algo semelhante ao fluxo 3, já que o Delete também precisa especificar um ID, então é algo condicional, mas considerando que os usuários colocassem uma id diferente a cada requisição (e se a id existisse), a taxa de sucesso seria de 100%, provavelmente.
## Agradecimentos:

Fabio Chorobura por me ajudar no uso das rotas pelo Jmeter.
Alisson Rodrigues por tentar me ajudar com a exclusão de usuários.