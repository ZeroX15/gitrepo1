## Nome do Projeto
Testando a API de usuários do Serverest

## Resumo
O teste será feito para que um vendedor possa se cadastrar no Marketplace, veremos se essa parte está funcionando.

## Pessoas envolvidas
- Marcelo José

## Funcionalidades/Métodos a serem utilizados:
Get - usuários e usuários/id
Post - usuários
Put - usuários/id
Delete - usuários/id

## Local do teste
Os testes serão realizados em home office

## Recursos necessários
Nenhum recurso especial necessário, apenas internet

## Critérios utilizados
- Verificar se todos os status codes mostrados no swagger são mostrados em cada situação
- Se aparecer um status code diferente, registrar no mapa mental

## Riscos
- Falta de internet
- Falta de energia

Solução: esperar voltar

## Como os resultados finais serão divulgados
Por meio de transmissão de tela mostrando cada item necessário

## Cronograma
Seg. a Sex. - 13:30 às 17:30