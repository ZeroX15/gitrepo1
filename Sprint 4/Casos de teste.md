## Situações:
- Os carrinhos retornados são únicos por usuário
- Cada usuário pode ter apenas 1 carrinho, que há um vínculo com o token do usuário
- É possível buscar carrinhos específicos pelo id
- Ao concluir uma compra o carrinho é excluído, que é o carrinho vinculado ao token
- Ao cancelar uma compra ocorre o mesmo, mas ele reabastece a quantidade dos produtos que estavam no carrinho

## Casos de teste
- Caso nº1: verificar a possibilidade de dois usuários terem o mesmo id de carrinho
- Caso nº2: verificar se um usuário pode ter mais de um carrinho
- Caso nº3: verificar se o estoque é alterado com a criação e/ou exclusão de um carrinho
- Caso nº4: verificar se é possível adicionar produtos com quantidade esgotada
- Caso nº4.1: verificar se o carrinho é criado caso a quantidade a ser adicionada for zero ou menor
- Caso nº4.2: verificar se o carrinho é criado caso a quantidade for um número flutuante
- Caso nº4.3: verificar se o carrinho é criado apenas com o array produtos vazio
- Caso nº5: verificar se o carrinho é excluído caso o usuário for excluído também

## Resultados
- Caso nº1: eu testei criando carrinhos de dois usuários diferentes com o mesmo produto, acabou que o carrinho sempre vai gerar um id diferente para cada carrinho, não há muito o que fazer já que o id fica armazenado dentro da api e só é acessível se eu coloco o id pra buscar por outros carrinhos.

- Caso nº2: Após criar um carrinho em um dos usuários, eu tentei criar um outro carrinho removendo um produto como se eu fosse editar o carrinho, mas a api não permitiu e mostrou a mensagem que não era permitido ter mais de 1 carrinho. Pode ser considerado uma sugestão de melhoria, mas, poderia ter um PUT na rota de carrinhos, assim seria possível editar sem ter que ficar criando e apagando, criando e apagando, de novo e de novo.

- Caso nº3: Ao cadastrar um carrinho, a quantidade diminuiu, por exemplo: Há 10 maçãs no estoque e eu coloquei 5 no carrinho, lá no estoque deveria estar 5 maçãs, e foi o que aconteceu. Já quando a compra foi cancelada, o estoque foi realmente abastecido. E quando foi concluída a compra (ao criar novamente o carrinho), o estoque se manteve, como se X produtos fossem realmente retirados do estoque.

- Caso nº4: Bom, não foi necessário esvaziar o estoque inteiro, ao criar um carrinho que requer um número maior de quantidade de um produto do que há no estoque, a API não permitiu criar devido a insuficiência de estoque. Isso me lembrou de uma coisa, irei testar algo similar.

- Caso nº4.1: A API não permitiu criar um carrinho "vazio" em quantidade, ele deu a obrigatoriedade de que a quantidade requerida fosse um número positivo, ou seja, independentemente se eu colocasse 0 ou -1, -3, etc.
Sinto que isso me deu mais uma ideia.

- Caso nº4.2: Como eu imaginei, ele também não permitiu usar números que não fossem inteiros, como 0,5 (meio) ou 2,3 (dois vírgula três).

- Caso nº4.3: Foi meio óbvio mas ele deu a mensagem de erro, pois os parâmetros id e quantidade são obrigatórios para o funcionamento.

- Caso nº5: Ao tentar excluir um usuário com carrinho cadastrado, ele não permitiu excluir nem o usuário, nem o carrinho.