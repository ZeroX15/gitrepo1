function retornaMaior(valor1, valor2) {
    let num1 = valor1
    let num2 = valor2
    let maior
    if (num1 > num2) {
        maior = num1;
    } else {
        maior = num2;
    }
    console.log(maior);
}

retornaMaior(5, 15);

//pode se tornar...

let valorMaior = max(3,5);
console.log(valorMaior);

function max(numero1, numero2) {
    if (numero1 > numero2)
        return numero1;
    else return numero2;
}

//Mas também pode ser assim...

let numMaior = valorMax(10, 6);
console.log(numMaior);

function valorMax(val1, val2) {
    return val1>val2 ? val1 : val2;
}