let hora = 14
if (hora >= 6 && hora <= 12){
    console.log("Bom dia");
} else if (hora > 12 && hora <= 18){
    console.log("Boa tarde");
} else {
    console.log("Boa noite");
}

let permissao = "diretor";
switch (permissao){
    case "comum":
        console.log("Usuário comum");
        break;

    case "gerente":
        console.log("Usuário gerente");
        break;
    
    case "diretor":
        console.log("Usuário diretor");
        break;

    default:
        console.log("Usuário inválido");
        break;
}