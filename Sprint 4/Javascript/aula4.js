/*
//For

for(let i = 1; i < 11; i++) {
    if (i == 1){
        console.log("Estou aprendendo", i, "vez");
    } else {
        console.log("Estou aprendendo", i, "vezes");
    }
}

//while
let i = 5;

while (i >= 1) {
    if (i % 2 !== 0) {
        console.log(i);
    }
    i--;
}

//Do..while
let i = 0
do {
    console.log("Carregando...");
    i++
} while (i < 10);
*/

//For..in
const pessoa = {
    nome: 'Rodrigo',
    idade: 25
};

for (let chave in pessoa) {
    console.log(chave, pessoa);
}

const cores = ["Vermelho", 'Azul', 'Verde'];

for (let indice in cores) {
    console.log(indice, cores[indice])
}

//For..of

for(let cor of cores){
    console.log(cor)
}