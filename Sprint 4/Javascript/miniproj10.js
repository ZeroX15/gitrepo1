exibirNumerosPrimos(100);

function exibirNumerosPrimos(limite) {
    for (let i = 2; i <= limite; i++) {
        if (i % 1 === 0 && i % i === 0) {
            if((i === 2 || i % 2 !== 0) && (i === 3 || i % 3 !== 0) && (i === 5 || i % 5 !== 0))
                console.log(i);
        }
    }
}