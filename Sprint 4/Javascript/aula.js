//variáveis
/*
let idade = 5;
console.log(idade);

let pais = "Brasil";
let moeda = "BRL";
let valor = "5.49";
*/
//constantes
/*
const valorIngressoAdulto = 20;
console.log(valorIngressoAdulto);
*/
//tipos primitivos
/*
let nome = 'Rafael';
let idade2 = 25;
let estaAprovado = true;
let sobrenome;
let corSelecionado = null;
*/

//objects

let pessoa = {
    nome: 'Rafael',
    idade: 25,
    estaAprovado: true,
    sobrenome: 'de Souza',
    temCNH: false,
    cargo: 'freelancer',
    salario: 2500.37
};

//Array

let familia = [26,45,50,17];

let coleguinha = ['Pedro', 20, 'São Paulo'];
console.log(coleguinha);