# Testes exploratórios

Os testes exploratórios são uma abordagem de teste de software que se baseia na exploração dinâmica do aplicativo ou sistema, sem a necessidade de documentação de casos de teste predefinidos. Essa técnica de teste é altamente eficaz na descoberta de defeitos, especialmente em cenários complexos e em constante mudança.

## Eles se diferenciam dos testes baseados em roteiros em alguns aspectos:

- Os testes exploratórios não são planejados desde o início, são planejados durante a execução do teste;
- Os (testes) exploratórios precisam seguir um cronograma específico, um limite de tempo;
- Os exploratórios servem para encontrar erros, não previní-los;
- Não há formas de automatizar os testes exploratórios;
- Há resultados mais detalhados além do "sucesso" ou falha dos que são baseados em roteiro;
- Não há um passo a passo definido, apenas um norte inicial.

## Estratégias:

Podem ser utilizadas algumas estratégias que facilitem a realização dos testes, isso também depende das habilidades e interpretação do testador.

Estratégias como Personas (Que são a criação de perfis fictícios de usuários, ou seja, pensar como o usuário pensaria); Tours (Que funciona como um planejamento inicial e básico onde pode ou não ser alterado constantemente para atingir melhores resultados); Shoe Test (a grosso modo seria literalmente usar um sapato ou um livro para apertar várias teclas ao mesmo tempo na hora de preencher formulários, por exemplo, porque alguns softwares tendem a gerar erros quando várias teclas são pressionadas ao mesmo tempo); Get one, take one (seria realizar o mesmo teste em ambientes diferentes e ver se há algum erro); podem ser utilizadas para os testes exploratórios.


Referências: Curso Udemy Testes Exploratórios por Wagner Costa.
