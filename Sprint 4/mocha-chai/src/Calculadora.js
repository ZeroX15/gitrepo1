export default class Calculadora {
    static soma(a, b){
        return a + b;
    }
    static sub(a, b){
        return a - b;
    }
    static mult(a, b){
        return a * b;
    }
    static div(a, b){
        return a / b;
    }
    static mod(a, b){
        return a % b;
    }
    static pow(a, b){
        return a ** b;
    }
    static rad(a, b){
        return a ** (1/b)
    }
    static fat(a){
        let init = a;
        let i = 2;
        let resultado= 1;
        while(a >= i){
            resultado *= (a-1);
            a--;
        }
        return resultado*init
    }
}