## Nome do Projeto

Testando a API de usuários, login e produtos do Serverest

## Resumo

Parte 1:
O teste será feito para que um vendedor possa se cadastrar no Marketplace

Parte 2:
O teste, após o usuário estar cadastrado, consiste em autenticar e mexer com os produtos (adicionar, editar, excluir...) no Marketplace da API

## Pessoas envolvidas

- Marcelo José

## Funcionalidades/Métodos a serem utilizados:

- Get /usuarios
- Get /usuarios/{_id}
- Post /usuarios
- Put /usuarios/{_id}
- Delete /usuarios/{_id}
- Post /login
- Get /produtos
- Post /produtos
- Get /produtos/{_id}
- Delete /produtos/{_id}
- Put /produtos/{_id}

## Local do Teste

Os testes serão realizados em home office

## Recursos necessários

Nada de especial, apenas energia e internet

## Critérios utilizados nos casos de teste

- Verificar se todos os status codes mostrados no Swagger aparecem em cada situação e registrar aqui
- Além de verificar se não é mostrado uma mensagem vazia, ou seja, se a API está funcionando devidamente 
- Em caso de status code diferente ou outros casos, registrar aqui, no mapa mental e no Jira, junto dos outros resultados

## Riscos
- Falta de internet
- Falta de energia

Solução: apenas esperar voltar

## Como os resultados finais serão divulgados

Aqui pelo readme e por transmissão de tela na apresentação da sprint

## Cronograma
Seg. a Sex. - 13:30 as 17:30, exceto feriados

## Resultados

- Get /usuarios obteve o status code 200
- Post /usuarios obteve os status codes 200 e 400 (e uma variante do 400, que requeria dados obrigatórios)
- Get /usuarios/{_id} obteve os status codes 200 e 400
- Put /usuarios/{_id} obteve os status codes 200, 201 e 400
- Delete /usuarios/{_id} obteve os status 200 e 405(?), ele deveria obter o status 400 também, o status 405 foi devido a não especificação do campo ID
- Post /login obteve os status codes 200 e 401 como no Swagger
- Get /produtos obteve o status code 200 como no Swagger
- Post /produtos obteve os status codes 201, 400 e 401 como no Swagger, o status code 403 não foi testado
- Get /produtos/{_id} obteve os status codes 200 e 400 como no Swagger
- Delete /produtos/{_id} obteve os status codes 200 e 401 como no Swagger, os status codes 400 e 403 não foram testados
- Put /produtos/{_id} obteve os status codes 201, 400 e 401, o status code 403 não foi testado e o status code 200 foi impossível de atingir, sendo isso um possível erro ou defeito
- Atualização: o status 200 do Put /produtos/{_id} foi atingido devido ao uso do id, estava com o id padrão de usuário e não o id do produto, isso causou o erro. Ao usar o id de produto, funcionou normalmente, como deveria.

Nota: todos os status continham suas mensagens específicas, incluindo aqueles que não estavam no swagger, ou seja, não mostravam apenas um status code e a mensagem vazia.