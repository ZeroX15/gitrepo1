User Story Nº??? Carrinho

Sendo um vendedor já cadastrado
Gostaria de registrar os produtos dentro de um carrinho do Marketplace do Serverest
Para que eu possa revisar todos os produtos a serem deduzidos do estoque

DoR
- Banco de dados e infraestrutua de desenvolvimento disponibilizados
- API de registro de carrinho implementada
- Ambiente de testes disponibilizado

DoD
- CRUD de cadastro de carrinho implementada
- Análise de testes cobrindo a rota de carrinho
- Matriz de rastreabilidade atualizada
- Automação de testes baseado na análise realizada

Acceptance Criteria
- Usuários sem autenticação não devem ser capazes de realizar ações na rota de Carrinho
- Caso não exista um ID de carrinho ao adicionar um produto, deverá ser criado um carrinho
- Gerar uma pergunta de confirmação de exclusão para carrinhos com mais de um produto
- Os testes executados deverão ter evidências
- A cobertura de testes deve se basear no Swagger e ir além, cobrindo cenários alternativos.