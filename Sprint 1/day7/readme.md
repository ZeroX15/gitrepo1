# Dia 7 da Sprint 1

## O que ocorreu hoje?

Comecei com os fundamentos de AWS:
- Quais as funções de quem trabalha na nuvem
- O que é a computação em nuvem
- Amazon EC2 e suas instâncias
*Nota: o curso é gigantesco, possívelmente terei que pular alguns assuntos no dia 8*

## Como o dia terminou?
Terminei vendo sobre o scaling do Amazon EC2, que envolve manipular a capacidade de instâncias dependendo da demanda.