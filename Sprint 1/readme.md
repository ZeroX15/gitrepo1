# Esta é a Sprint 1.

Aqui está um resumo de todos os dias que ocorreram nesta sprint.

## Dia 1

Comecei com os fundamentos do GIT:
- Criação de repositório
- Adição e remoção de arquivos para o repositório
- Salvamento/commit de arquivos
- Diferença entre versionamento distribuído e centralizado
- Quatro estágios de um arquivo de repositório

## Dia 2

Estudos sobre fundamentos do SCRUM:
- Modelo tradicional versus modelo ágil
- Fluxo do SCRUM
- Backlog e backlog da sprint

## Dia 3

Teste e QA parte 1:
- Como são as vagas para Teste e QA e a expansão delas
- Quem é apto para esse tipo de trabalho

## Dia 4

Teste e QA parte 2:
- Habilidades pessoais e interpessoais
- Trabalho em equipe
- Hard skills

## Dia 5

Teste e QA parte 3:
- Importância de realizar testes e os prejuízos que os bugs podem causar
- Diferença entre erro, defeito e falha
- 7 fundamentos do teste
- IEC/ISO 25010

## Dia 6:

Fundamentos de Teste de Software:
- Teste ponta a ponta
- Teste de Integração
- Teste de unidade

## Dia 7:

Fundamentos de AWS parte 1:
- Funções de quem trabalha na nuvem
- Computação em nuvem
- Amazon EC2 e suas instâncias
- Scaling do Amazon EC2

## Dia 8:

Fundamentos de AWS parte 2:
- Elastic Load Balancing
- AWS Lambda
- Amazon SNS e SQS
- Regiões e zonas de disponibilidade da AWS

## Dia 9:

Fundamentos de AWS parte 3:
- Locais de Borda e Amazon CloudFont
- AWS Management Console, AWS CLI e SDKs
- AWS Elastic Beanstalk
- AWS CloudFormation

## Dia 10:

***Apresentação da Sprint***


Autor: [Marcelo José](https://gitlab.com/ZeroX15)