# Dia 3 da Sprint 1

## O que ocorreu hoje:

Comecei vendo as aulas da Udemy sobre Teste e QA, no início era apenas:
- As vagas de Teste e QA e a expansão dessas vagas
- Quem pode fazer
- Os assuntos são meio extensos, por isso a trilha divide o curso em três dias

Tivemos uma daily (ou quase) para sanar dúvidas, ocorreu uns problemas e confusões sobre ambas as equipes, mas está sendo resolvido, criamos um grupo tanto pro Discord quanto pro Whatsapp para nos organizarmos. Também tivemos uma reunião com a equipe da Compass para sanar outras dúvidas

## Como o dia terminou:

Eu fiz uns ajustes na documentação com a extensão correta, antes estava .txt, agora está .md