# Dia 5 da Sprint 1

## O que ocorreu hoje?

Finalização do curso de Teste e QA pela Udemy, a segunda parte englobava:
- Importância do teste e que problemas os bugs podem causar
- Diferença entre erro, defeito e falha
- Os 7 fundamentos do Teste
- IEC/ISO 25010
- Entre outros

## Como o dia terminou?

Ainda sobrou alguns assuntos complementares, mas nada tão urgente.

//OBS: preciso de um café
