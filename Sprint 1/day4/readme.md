# Dia 4 da Sprint 1

## O que ocorreu hoje?

Continuei os estudos sobre Teste e QA, assuntos ainda introdutórios como:

- Habilidades Pessoais
- Habilidades Interpessoais
- Trabalho em equipe
- Hard Skills

## Como o dia terminou?

Estou quase terminando a primeira parte desse curso de Teste e QA, creio que amanhã eu consiga terminar tudo.
