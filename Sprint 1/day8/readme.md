# Dia 8 da Sprint 1

## O que ocorreu hoje?

Continuei os estudos sobre a AWS, mais específicamente na computação em nuvem:
- O direcionamento de tráfego com ELB (Elastic Load Balancing)
- AWS Lambda e a computação sem servidores
- Sistema de mensagem e redirecionamento (Amazon SNS e Amazon SQS)
- As regiões e zonas de disponibilidade da AWS

Também tivemos uma reunião com o SM para esclarecer o andamento do curso de AWS e o futuro das outras sprints

## Como o dia terminou?

Ainda tenho mais um dia para poder ver o máximo que puder sobre AWS.