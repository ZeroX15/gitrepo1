# Dia 9 da Sprint 1

## O que ocorreu hoje?

Continuei os estudos de AWS:
- Regiões e Zonas de Disponibilidade
- Locais de Borda e Amazon CloudFont
- AWS Management Console, AWS CLI e SDKs
- AWS Elastic Beanstalk
- AWS CloudFormation

## Como o dia terminou/o que eu farei amanhã?

Eu tive um pequeno imprevisto em relação à queda de energia, o que me impossibilitou de participar da daily.
Pretendo juntar toda a informação dos readme anteriores em um único readme resumido.